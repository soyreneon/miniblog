import {NavLink} from "react-router-dom";
import './styles/OffCanvas.scss'
const OffCanvas = ({idref}) => {
    return ( 
        <div className="offcanvas offcanvas-start" tabIndex="-1" id={idref} aria-labelledby="offcanvasExampleLabel">
        <div className="offcanvas-header">
            <h5 className="offcanvas-title" id="offcanvasExampleLabel">Mi Sitio</h5>
            <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div className="offcanvas-body">
            <ul className="list-group list-group-flush">
                <li>
                    <NavLink className="list-group-item"  to="/" activeClassName="active" exact>
                        Inicio
                    </NavLink>
                </li>
                <li>
                    <NavLink className="list-group-item" to="/acerca" activeClassName="active">
                        Acerca
                    </NavLink>
                </li>
                <li>
                    <NavLink className="list-group-item" to="/temas" activeClassName="active">
                        Temas
                    </NavLink>
                </li>
            </ul>
            <div className="my-4">Más</div>
            <ul className="list-group list-group-flush">
                <li>
                    <NavLink className="list-group-item" to="/recomendaciones" activeClassName="active">
                        Recomendaciones
                    </NavLink>
                </li>
                <li>
                    <NavLink className="list-group-item" to="/no-existe" activeClassName="active">
                        Error 404
                    </NavLink>
                </li>
            </ul>
        </div>
        </div>
     );
}
 
export default OffCanvas;