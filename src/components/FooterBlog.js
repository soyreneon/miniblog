import React from 'react'
import Loading from "../components/Loading"
import Contacto from "../components/Contacto"
import './styles/FooterBlog.css'
import {
    Switch,
    Route
  } from "react-router-dom";
  
const FooterBlog = () => (
<div className="footer">
    <div className="container">
    <footer className="row  py-5">
        <div className="col-sm-12 col-md-4">
            <h1>Mi Sitio</h1>
            <p>Lorem ipsum dolor sit am et,
                consectetur adipiscing elit. Phasellus
                quis malesuada massa.</p>
            <span>© 2021, All rights reserved.</span>
        </div>
        <div className="col-sm-12 col-md-5 offset-md-3">
        <Switch>
            <Route path="/acerca">
                    <Contacto />
            </Route>
            <Route component={Loading} />
        </Switch>
        </div>
    </footer>
    </div>
</div>
)

export default FooterBlog