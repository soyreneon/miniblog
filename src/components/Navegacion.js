//import { render } from '@testing-library/react';
import React from 'react'
import './Navegacion.css'
import {NavLink} from "react-router-dom";
import IconosRedes from './IconosRedes'
import OffCanvas from './OffCanvas'

import {FontAwesomeIcon}  from '@fortawesome/react-fontawesome'

//import fontawesome from '@fortawesome/fontawesome'
/*import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import {  faFacebook } from '@fortawesome/free-brands-svg-icons'
*/

const Navegacion = () => (
    <div className="row mb-4 mb-md-0">
        <div className="col-sm-12 col-md-3">
            <div className="redes text-center text-md-start">
            <IconosRedes 
                iconName="facebook"
                link="https://www.facebook.com/ReneSoon" />
            <IconosRedes
                iconName="twitter"
                link="https://twitter.com/SoyReneOn" />
            <IconosRedes
                iconName="instagram"
                link="https://instragram.com/SoyReneOn" />
            <IconosRedes iconName="spotify"
                link="https://open.spotify.com/artist/0SD56zsT0YyyYBz4l1flJw" />
            <IconosRedes iconName="deezer" />
            </div>
        </div>
        <div className="d-none d-md-block col-md-6">
            <nav className="row text-center navigation-style">
                <li className="col-3">
                    <NavLink to="/" activeClassName="active" exact>
                        Inicio
                    </NavLink>
                </li>
                <li className="col-3">
                    <NavLink to="/acerca" activeClassName="active">
                        Acerca
                    </NavLink>
                </li>
                <li className="col-3">
                    <NavLink to="/temas" activeClassName="active">
                        Temas
                    </NavLink>
                </li>
                <li className="col-3 dropdown">
                        <a className="dropdown-togglenoused" href="/" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            Más 
                            <FontAwesomeIcon icon='chevron-down' className="ps-1" size="xs" />
                        </a>
                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li>
                                <NavLink to="/recomendaciones" className="dropdown-item" activeClassName="active">
                                    Recomendaciones
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="dropdown-item" to="/no-existe" activeClassName="active">
                                    Error 404
                                </NavLink>
                            </li>
                        </ul>
                </li>
            </nav>
        </div>
        <div className="col-6 col-md-3 d-md-none">
            <a className="link-gray" data-bs-toggle="offcanvas" href="#offcanvasMenu" role="button" aria-controls="offcanvasExample">
                <FontAwesomeIcon icon='bars' size="lg" />
            </a>
            <OffCanvas idref="offcanvasMenu" />
        </div>
        <div className="col-6 col-md-3 mt-md-4 text-end text-gray">
            Ready for the future!
            { /* <FontAwesomeIcon icon='sort-down' size="1x" /> */ }
        </div>
    </div>
)

export default Navegacion
