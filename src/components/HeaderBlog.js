import React from "react"

const HeaderBlog = ({ background, logo }) =>(
    <div className="" style={{
        background: `url(${background})`,
        backgroundRepeat: 'repeat',
        width: '100%', height: '250px',
        backgroundPosition: 'center 60%' }}>
        <div className="container">
            <header className="position-relative"
                    style={{
                        width: '100%', height: '250px' }}>
                <img src={logo} className="App-logo position-absolute top-50 start-50 mt-4 translate-middle rounded-circle" alt="logo" />
            </header>
        </div>
    </div>
)

export default HeaderBlog