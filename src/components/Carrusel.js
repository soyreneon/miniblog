const Carrusel = ({fotos}) => {
    const listfotos = fotos.map( (foto, i) =>
        <div key={foto.id} className={ `carousel-item ${i===0 && "active" } `} data-bs-interval="1000">
            <img src={ `https://picsum.photos/id/${foto.id}/300/300/`} className="d-block w-100" alt="slide" />
        </div>
        );
    return ( 
        <div id="carouselExampleInterval" className="carousel slide" data-bs-ride="carousel">
        <div className="carousel-inner">
            {listfotos}
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
        </button>
        </div>
     );
}
 
export default Carrusel;