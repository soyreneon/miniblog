import './styles/IconosRedes.scss'
import {FontAwesomeIcon}  from '@fortawesome/react-fontawesome'


const IconosRedes = ({iconName, link}) => (
    <div className="icon-div d-inline-block me-3 mt-4">
        <a href={ link || '#' } target="_blank" rel="noreferrer">
            <FontAwesomeIcon icon={['fab', `${iconName}`]} size="1x" />
        </a>
    </div>
)

export default IconosRedes