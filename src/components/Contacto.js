import React, { useState, useRef } from 'react';
import { useForm } from "react-hook-form";
import ReCAPTCHA from "react-google-recaptcha";

/*
recaptcha google
key
6Lfii04bAAAAANYwRUoY_zeGI5wghRf5oiiae2io
secret
6Lfii04bAAAAAKjyx8zoFIsJzJbihLh5-Z5PtFIS
*/
const Contacto = () => {
  //const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [entrada, setEntrada] = useState([])
  const [captchaIs, setCaptchaIs] = useState(false)
  // un hook para referenciar el captcha
  const captchavalor = useRef(null)
  const onSubmit = (data, e) => {
    if (captchavalor.current.getValue()){
      setCaptchaIs(true)
      console.log(data);
      // aqui deberia ir un post a una api rest para guardar los datos
      setEntrada([
        ...entrada,
        data
      ])
      e.target.reset();
      console.log('no es robot')
    }else{
      setCaptchaIs(false)
      console.log('debe aceptar el catpcha')
    }
  }
  const onChangeCaptcha = (value) => {
    setCaptchaIs(true)
    console.log("Captcha value:", value);
    if (captchavalor.current.getValue()){
      console.log('no es robot')
    }
  }
  ///console.log(watch("nombre")); // watch input value by passing the name of it
    /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
    return ( <>
    <h4 className="mt-5 mt-md-0 mb-4 text-center text-md-start">Suscribete al newsletter</h4>
    <form onSubmit={handleSubmit(onSubmit)} className="mb-3">
      {/* register your input into the hook by invoking the "register" function */}
      <div className="mb-3">
          <input
            placeholder="nombre"
            className="form-control"
            defaultValue="nombre"
            {...register("nombre",
            {
              maxLength: 50
            })} />
      </div>
      {errors.nombre && errors.nombre.type === "maxLength" && (
        <span className="d-block mb-3">Max length excedido</span>
      )}
      
      {/* include validation with required or other standard HTML validation rules */}
      <div className="mb-3">
          <input
          placeholder="mail"
          className="form-control"
          {...register("correo",
            { required: true,
              maxLength: 40
            })} />
      </div>
      {/* errors will return when field validation fails  */}
      {/*errors.correo && <span className="d-block mb-3">Este campo es obligatorio</span>*/}
      {errors.correo && errors.correo.type === "required" && (
        <span className="d-block mb-3">Este campo es obligatorio</span>
      )}
      {errors.correo && errors.correo.type === "maxLength" && (
        <span className="d-block mb-3">Max length excedido</span>
      )}

      <ReCAPTCHA
        ref={captchavalor}
        sitekey="6Lfii04bAAAAANYwRUoY_zeGI5wghRf5oiiae2io"
        onChange={onChangeCaptcha}
        theme="dark"
      />
      {!captchaIs && 
      <span className="d-block mb-3">Debes aceptar el captcha</span>}

      <input className="mt-3 btn btn-light" type="submit" />
    </form>
    <ul>
      {
        entrada.map( (item,i) => 
          <li key={i}>{item.correo}</li>
          )
      }
    </ul>
    </> );
}
 
export default Contacto;