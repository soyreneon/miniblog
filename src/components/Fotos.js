//import react from 'react'
import {Link} from "react-router-dom";

const Fotos = ({fotos}) => (
    <div className="row justify-content-center">
    {
        fotos.map( foto => { //fotos.slice(0, 5).map( foto =>
            // el key lo pide en iteracioes para tener control de los datos iterados modificados, eliminados etc
            const imgnumber = foto.id * Math.floor((Math.random()*90)+1)
            return(
                <div key={foto.id} className="card col-1 mt-2" style={{ width: '32rem' }} >
                    <img src={ `https://picsum.photos/id/${imgnumber}/300/270/` /*foto.url // foto reemplazada por picsum para mejor imagen */} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">Card title</h5>
                        <p className="card-text">{ `${foto.title}   `}</p>
                        <div className="d-flex flex-row-reverse d-end">
                        <Link to={
                            {
                                pathname: `/fotos/${foto.id} `,
                                customImg: imgnumber
                            }
                        } className="link-btn-more ">Ver más</Link>
                        </div>
                    </div>
                </div>
            )
        })
    }
    </div>
)

export default Fotos