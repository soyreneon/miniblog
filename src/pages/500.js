import React from "react"

const FatalError = ({ msg }) =>(
    <div className="text-center my-5 ">
        <h1>Error 500</h1>
        <p>{msg}</p>
    </div>
)

export default FatalError