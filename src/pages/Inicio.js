import useFetch from '../hooks/useFetch'
import Fotos from '../components/Fotos'
import FatalError from './500'
import Loading from "../components/Loading"
import Carrusel from "../components/Carrusel"

const Inicio = () => {
    const {data, error, loading} = useFetch('https://jsonplaceholder.typicode.com/photos/?_limit=4')
    const {data: dataCarrusel, error: errorCarrusel, loading: loadingCarrusel} = useFetch('https://jsonplaceholder.typicode.com/photos/?_limit=10')
    const {data: dataRcards, error: errorRcards, loading: loadingRcards} = useFetch('https://jsonplaceholder.typicode.com/photos/?_start=10&_limit=4')
    //{data as dataCarrusel, error as errorCarrusel , loading as loadingCarrusel} = useFetch(MyUrl) 
    //console.log(data);
    //console.log(dataCarrusel);

/*    if (loading)
        return <Loading />
    if (error)
        return <FatalError msg="Hubo un error al realizar la solicitud" />
*/
    return <div className="row pb-3">
        <div className="col-12 col-md-3">
        {
        loading &&
            <Loading /> 
        }
        {
        error &&
            <FatalError msg="Hubo un error al realizar la solicitud" />
        }
        { data &&
            <Fotos fotos={data} />
        }
        </div>
        <div className="col-l2 col-md-6">
            <div className="card bg-dark text-white">
                <img src={`https://picsum.photos/id/${ Math.floor((Math.random()*100)+1)}/500/500/`} className="card-img" alt="..." style={{minHeight: '300px'}} />
                <div className="card-img-overlay">
                    <div className="bg-white text-black text-center position-absolute bottom-0 start-50 translate-middle-x p-4">
                        <h2 className="card-title text-reset">Hola</h2>
                        <p className="card-text">Bienvenido a mi sitio de prueba.</p>
                    </div>
                </div>
            </div>
            <div className="my-5 mx-5 text-center">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries
            </div>
            
            {
            loadingCarrusel &&
                <Loading /> 
            }
            {
            errorCarrusel &&
                <FatalError msg="Hubo un error al realizar la solicitud" />
            }
            { dataCarrusel &&
                <Carrusel fotos={dataCarrusel} />
            }
        </div>
        <div className="col-12 col-md-3">
        {
        loadingRcards &&
            <Loading /> 
        }
        {
        errorRcards &&
            <FatalError msg="Hubo un error al realizar la solicitud" />
        }
        { dataRcards &&
            <Fotos fotos={dataRcards} />
        }
        </div>
        
    </div>
}

export default Inicio