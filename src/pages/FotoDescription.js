import React from 'react'
import { useParams, useLocation } from 'react-router-dom'
import useFetch from '../hooks/useFetch'
import FatalError from './500'
import Loading from "../components/Loading"

const FotoDescription = ({customImg}) => {
    const {id} = useParams()
    const location = useLocation()
    console.log(location.customImg)

    const {data, error, loading} = useFetch(`https://jsonplaceholder.typicode.com/photos/${id}`)
    console.log(data)

    if (loading)
        return <Loading />
    if (error)
        return <FatalError msg="Hubo un error al realizar la solicitud" />

    return(
    <div className="container">
    <div className="card mb-3" style={{ maxWidth: '1240px' }}>
        <div className="row g-0">
            <div className="col-md-4">
                <img src={ `https://picsum.photos/id/${location.customImg || id}/300/300/` /*data.url // foto reemplazada por picsum para mejor imagen */} className="img-fluid" alt={`esto es un  ${location.customImg || id}`}  />
            </div>
            <div className="col-md-8">
                <div className="card-body">
                    <h5 className="card-title">Card title</h5>
                    <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>
    </div>
    )

}

export default FotoDescription