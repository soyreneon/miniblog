import React from "react"

function NotFound(){
    return (
        <div className="text-center my-5 ">
        <h1>Error 404</h1>
        <p>Esta página no se encuentra. Contacte al administrador.</p>
    </div>)
}

export default NotFound