import logo from './assets/logoo.png';
import background from './assets/pexels-burst2.jpg';
import './App.scss';
import HeaderBlog from './components/HeaderBlog'
import FooterBlog from './components/FooterBlog'
import Navegacion from './components/Navegacion'

import NotFound from "./pages/NotFound";
import Inicio from './pages/Inicio'
import Acerca from './pages/Acerca'
import Recomendaciones from './pages/Recomendaciones'
import Temas from './pages/Temas'
import FotoDescription from './pages/FotoDescription'

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//import 'bootstrap/dist/js/bootstrap.js';
//import $ from 'jquery';
//import Popper from 'popper.js';

import './imports/font-awesome';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <Router>
    <HeaderBlog 
      background={background}
      logo={logo}
    />
    <div className="App container">
      <Navegacion />
      <Switch>
        <Route path="/fotos/:id">
          <FotoDescription />
        </Route>
        <Route path="/acerca">
          <Acerca />
        </Route>
        <Route path="/temas">
          <Temas />
          { /*<div className="shadow-lg p-3 mb-5 bg-body rounded">Larger shadow</div> */ }
        </Route>
        <Route path="/recomendaciones">
          <Recomendaciones />
        </Route>
        <Route path="/" exact>
          <Inicio  />
        </Route>
        <Route component={NotFound} />  
      </Switch>
    </div>
    <FooterBlog />
    </Router>
  );
}

export default App;
