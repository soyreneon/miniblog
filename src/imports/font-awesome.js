import { library } from '@fortawesome/fontawesome-svg-core'
import { fab, faFacebook } from '@fortawesome/free-brands-svg-icons'
import { faCheckSquare, faSortDown, faChevronDown,faBars } from '@fortawesome/free-solid-svg-icons'
//import {  } from '@fortawesome/free-brands-svg-icons'
// faFacebook esta implicito en el import de fab, ya que es todo el paquete

library.add(fab, faCheckSquare, faSortDown, faChevronDown, faFacebook, faBars)
